import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//my imports
import { LayoutComponent } from './layout.component';
import { SharedModule } from '../shared.module';
import { TopbarComponent } from '../topbar/topbar.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { FooterComponent } from '../footer/footer.component';
import { MenuComponent } from '../sidebar/menu/menu.component';
import { MenuitemComponent } from '../sidebar/menu/menuitem.component';

@NgModule({
  declarations: [
    LayoutComponent,
    TopbarComponent,
    SidebarComponent,
    FooterComponent,
    MenuComponent,
    MenuitemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule { }
