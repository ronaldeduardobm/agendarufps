import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//My imports
import { PrimengModule } from './primeng.module';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    PrimengModule
  ],
  exports: [
    CommonModule,
    PrimengModule
  ]
})
export class SharedModule { }
