
export interface EventAssistant {
  name:string;
  location:string;
  topic:Topic[];
  id_appointment:number;
  appointment_start:string;
  url_meet:string;
  day:string;
  hour:string;
  asistant:Asistant[];
  availability:Availability
}

interface Availability {
weekdays:WeekDays[];
}
interface WeekDays  {
  id_week: number,
  name: string,
  state: boolean,
}
interface Asistant{
  name:string;
}
interface Topic {
  name: string,
  version: number,
  id_topic: number,
  created_at: string,
  updated_at: string,
  created_by: string
}
