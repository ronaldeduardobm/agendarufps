import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//my imports
import { LayoutComponent } from './shared/layout/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { EventComponent } from './pages/event/event.component';
import { AssistantsComponent } from './pages/assistants/assistants.component';
import { TopicComponent } from './pages/topic/topic.component';
import { AvailabilityComponent } from './pages/availability/availability.component';
import { ReportComponent } from './pages/report/report.component';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { ThanksComponent } from './pages/thanks/thanks.component';
import { AuthGuard } from './auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path:'app',
    component: LayoutComponent,
    children: [
      {path: 'eventos', component:EventComponent,canActivate: [AuthGuard]},
      {path: 'asistentes', component:AssistantsComponent,canActivate: [AuthGuard]},
      {path: 'temas', component:TopicComponent,canActivate: [AuthGuard]},
      {path: 'disponibilidad', component:AvailabilityComponent,canActivate: [AuthGuard]},
      {path: 'informes', component:ReportComponent,canActivate: [AuthGuard]}
    ]
  },
  {path: 'home', component:HomeComponent},
  {path: 'login', component:LoginComponent},
  {path: 'thanks', component:ThanksComponent},
  {path: 'appointment/:param1/:param2', component:AppointmentComponent},
  {path: 'notFound', component:NotFoundComponent},
  //{path: '**', redirectTo: '/notFound'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
