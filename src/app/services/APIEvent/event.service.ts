import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private host = environment.host;
  private urlBaseEvent = environment.eventBaseEndpoint;
  private urlBaseEventPublic = environment.eventBaseEndpointPublic;
  private urlBaseUser = environment.userBaseEndpoint;
  constructor(private http: HttpClient) { }

  getEvents(nickname:string) {
    console.log(this.host+this.urlBaseEvent);
    console.log(this.host+this.urlBaseEvent);
      const token = localStorage.getItem('auth_token');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.host+this.urlBaseEvent+nickname+'/', {headers});
  }

  getEventByName(name:string,nickname:string){
    console.log('getEventByName');
    console.log(this.host+this.urlBaseEvent+name+'/');
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.get(this.host+this.urlBaseEvent+nickname+'/'+name+'/', {headers});
  }

  getEventTopicsPublic(nickname:string, eventName:string){
    console.log('getEventTopicsPublic');
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'/topics');
  }
  getEventPublic(nickname:string, eventName:string){
    console.log('get api getEventPublic',nickname,eventName);
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'/');
  }

  getEventPublicIntervals(nickname:string, eventName:string,date:string){
    console.log('getEventPublicIntervals');
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'?date='+date);
  }

  postEvent(event: any,nickname:string) {
    console.log('postEvent');
    console.log(event);
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.post(this.host+this.urlBaseEvent+nickname+'/', event, { headers });
  }

  deleteEvent(nickname:string,eventName:string) {
    console.log('deleteEvent');
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.delete(this.host+this.urlBaseEvent+nickname+'/'+eventName+'/', { headers });
  }
  updateEvent(nickname:string,event:any) {
    console.log('updateEvent',event);
    console.log('nickname',nickname);
    console.log('nombre del evento ',event.name);
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.put(this.host+this.urlBaseEvent+nickname+'/'+ event.name +'/',event, { headers });
  }


}
