import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//my imports
import { SharedModule } from '../../shared/shared.module';
import { EventCardComponent } from './event-card/event-card.component';


@NgModule({
  declarations: [
    EventCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    EventCardComponent
  ]
})
export class EventModule { }
