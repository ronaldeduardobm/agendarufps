import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private host = environment.host;
  private loginGoogle=this.host+environment.loginGoogleBaseEndpoint;

  constructor( private http: HttpClient) { }

  getGoogleLoginURL() {
    console.log(this.loginGoogle);
    return this.http.get(this.loginGoogle);
  }

  getToken(code: string) {
    console.log('code gettoken ' + code);
    return this.http.get(this.host+'/auth/callback?code=' + code);
  }

}
