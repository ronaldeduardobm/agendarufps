import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Sidebar } from 'primeng/sidebar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent {
  @ViewChild('sidebarRef') sidebarRef!: Sidebar;
  closeCallback(e:any): void {
    this.sidebarRef.close(e);
}

sidebarVisible: boolean = false;
}
