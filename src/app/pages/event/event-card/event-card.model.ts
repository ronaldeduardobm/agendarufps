export interface EventCard {
  title: string;
  location: string;
  description: string;
  url: string;
  time: string;
}
