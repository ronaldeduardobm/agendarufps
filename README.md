
#  ![Logo](src/assets/images/logo-white.svg)

El proyecto propone desarrollar una plataforma accesible y fácil de usar que integre las cuentas institucionales de Google con el servicio de Google Calendar para facilitar la programación y gestión de asesorías entre profesores y estudiantes en la Universidad Francisco de Paula Santander (UFPS). Este sistema permitirá automatizar la reserva de citas, reduciendo así los errores humanos y las ineficiencias asociadas con los métodos manuales.

## Tecnologías Utilizadas

- **Frontend**:  <img src="https://angular.io/assets/images/logos/angular/angular.svg" alt="Angular" width="20" height="20"> Angular
- **UI Framework**: <img src="https://i0.wp.com/www.primefaces.org/wp-content/uploads/2018/05/primeng-logo.png?fit=300%2C300&ssl=1" alt="PrimeNG" width="16" height="16"> PrimeNG
- **Backend**: Asegúrate de configurar el host para la comunicación con el backend según las necesidades de tu entorno de desarrollo y producción.


## Estructura de carpetas
```bash
├───.angular
├───node_modules 
└───src
    ├───app
    │   ├───pages
    │   │   ├───appointment
    │   │   ├───assistants
    │   │   ├───availability
    │   │   │   ├───avai-card
    │   │   │   └───time
    │   │   ├───dashboard
    │   │   ├───event
    │   │   │   └───event-card
    │   │   ├───home
    │   │   ├───login
    │   │   ├───not-found
    │   │   ├───report
    │   │   ├───thanks
    │   │   └───topic
    │   │       └───topic-card
    │   ├───services
    │   │   ├───APIAppointment
    │   │   ├───APIAvailability
    │   │   ├───APIEvent
    │   │   ├───APIReport
    │   │   ├───APITopic
    │   │   ├───APIUser
    │   │   └───Login
    │   └───shared
    │       ├───footer
    │       ├───layout
    │       │   └───service
    │       ├───sidebar
    │       │   └───menu
    │       └───topbar
    └───assets
        ├───environments
        └───images
```
**Pages** Contiene todos los componetes necesarios para la vista de la aplicación, cada carpeta contiene un archivo .ts, .html y .css para su respectiva vista; Evento, Temas, Disponibildad, Estadistica y Agendados, home, login y error404.
**Services** Contiene los servicios necesarios para la comunicación con el backend, cada carpeta contiene un archivo .ts para su respectivo servicio; Evento, Temas, Disponibildad, Estadistica y Agendados, Usuario y Login.
**Shared** Contiene los componentes que se comparten en toda la aplicación, como el footer, sidebar y topbar.
**Assets** Contiene las imagenes y los archivos de configuración de la aplicación.

## Configuración Inicial

Para poner en marcha el proyecto, sigue los pasos a continuación:

**Clonar el Repositorio**
```bash
   git clone https://gitlab.com/ronaldeduardobm/agendarufps.git
   cd AgendarUFPS
```
## Instalar Dependencias

Asegúrate de tener instalado [Node.js](https://nodejs.org/) y [npm](https://www.npmjs.com/) en tu sistema para poder instalar las dependencias del proyecto.

```bash
  npm install
  ```
## Configuración del Backend

Configura el host para la comunicación con el backend modificando el archivo de configuración *enviroments.ts* ubicado en la carpeta del proyecto *src -> assets -> enviroments -> enviroments.ts*

```typescript
export const environment = {
  production: false,
  host: 'http://localhost:8080/agendarbackend',
  hostFront: 'http://localhost:4200/agendarUFPS',
  eventBaseEndpoint: '/event/',
  eventBaseEndpointPublic: '/public/event/',
  AvailabilityBaseEndpoint: '/availability/',
  topicBaseEndpoint: '/topic/',
  userBaseEndpoint: '/user',
  appointmentBaseEndpoint: '/appointment/',
  loginGoogleBaseEndpoint: '/auth/url',
  reportBaseEnpoint:'/report-general/'
};
```

## Ejecutar la Aplicación

Para ejecutar la aplicación, abre una terminal en la raíz del proyecto y ejecuta el siguiente comando:

```bash
  ng serve
```
Navega a `http://localhost:4200/` en tu navegador para ver la aplicación en ejecución.

## Desarrollado Por

- **Ronald Eduardo Benitez Mejia** - Código: 1151813
- **Correo**: [ronaldeduardobm@ufps.edu.co](mailto:ronaldeduardobm@ufps.edu.co)
- **Jhoser Fabian Pacheco Quintero** - Código: 1151807
- **Correo**: [jhoserfabianpq@ufps.edu.co](mailto:jhoserfabianpq@ufps.edu.co)

## Dirección del Proyecto

- **Director**: PhD. Marco Antonio Adarme Jaimes
- **Co-Directora**: PhD. Judith del Pilar Rodríguez Tenjo
