import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../layout/service/layout.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.css'
})
export class MenuComponent implements OnInit {

  model: any[] = [];

  constructor(public layoutService: LayoutService) { }

  ngOnInit() {
      this.model = [
          {
              items: [
                  { label: 'Eventos', icon: 'pi pi-fw pi-link', routerLink: ['eventos'] },
                  { label: 'Asistentes', icon: 'pi pi-fw pi-calendar-minus', routerLink: ['asistentes'] },
                  { label: 'Temas', icon: 'pi pi-fw pi-book', routerLink: ['temas'] },
                  { label: 'Disponibilidad', icon: 'pi pi-fw pi-clock', routerLink: ['disponibilidad'] },
                  { label: 'Informes', icon: 'pi pi-fw pi-chart-bar', routerLink: ['informes'] },
              ]
          }
        ]
  }

}
