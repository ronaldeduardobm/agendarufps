export interface AvaiCard {
  title: string;
  description: Description;
}

export interface Day{
  name: string;
  hours: string[];
}

export interface Description{
  days:Day[];
}
